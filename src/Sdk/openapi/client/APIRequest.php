<?php

namespace JyEleRetail\Sdk\openapi\client;

use JyEleRetail\Sdk\openapi\client\entity\AuthorizationToken;

class APIRequest
{
    /**
     *
     * @var APIId
     */
    var $apiId;
    
    /**
     *
     * @var
     */
    var $addtionalParams = [];
    
    /**
     *
     * @var
     */
    var $requestEntity;
    var $version = 3;
    
    /**
     *
     * @var
     */
    var $attachments = [];
    
    /**
     *
     * @var String
     */
    var $authCodeKey;
    
    /**
     *
     * @var String
     */
    var $accessToken;
    
    /**
     *
     * @var AuthorizationToken
     */
    var $authToken;
    var $encrypt;
}
