<?php

namespace JyEleRetail\Sdk\openapi\client\policy;

class ClientPolicy
{
    var $serverHost = "api-be.ele.me";
    var $httpPort = 80;
    var $httpsPort = 443;
    var $appKey;
    var $secKey;
    var $defaultContentCharset = "UTF-8";
    
}
