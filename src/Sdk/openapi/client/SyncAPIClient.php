<?php

namespace JyEleRetail\Sdk\openapi\client;

use JyEleRetail\Sdk\openapi\client\policy\ClientPolicy;
use JyEleRetail\Sdk\openapi\client\policy\RequestPolicy;

class SyncAPIClient
{
    var $clientPolicy;
    
    /**
     *
     * @param ClientPolicy $clientPolicy
     */
    function __construct(ClientPolicy $clientPolicy)
    {
        $this->clientPolicy = $clientPolicy;
    }
    
    public function execute($namespace, $apiName, $version, $requestDefiniation, $resultDefiniation, $accessToken)
    {
        $apiRequest                = new APIRequest();
        $apiId                     = new APIId($namespace, $apiName, $version);
        $apiRequest->apiId         = $apiId;
        $apiRequest->accessToken   = $accessToken;
        $apiRequest->requestEntity = $requestDefiniation;
        return $this->send($apiRequest, $resultDefiniation, new RequestPolicy());
    }
    
    public function send($url, $data)
    {
        $retry = 0;
        do {
            $url  = sprintf('%s?retry=%s', $url, $retry);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($curl);
            $info   = curl_getinfo($curl);
            curl_close($curl);
            if (isset($info['http_code']) && 200 == $info['http_code']) {
                break;
            }
            $retry++;
        } while ($retry < 10);
        return $output;
    }
    
    private function generateRequestPath(APIRequest $request, RequestPolicy $requestPolicy, ClientPolicy $clientPolicy)
    {
        $urlResult = "";
        if ($requestPolicy->accessPrivateApi) {
            $urlResult = "/api";
        } else {
            $urlResult = "/openapi";
        }
        
        $defs = [
            $urlResult,
            "/",
            $requestPolicy->requestProtocol,
            "/",
            $request->apiId->version,
            "/",
            $request->apiId->namespace,
            "/",
            $request->apiId->name,
            "/",
            $clientPolicy->appKey,
        ];
        
        return implode($defs);
    }
    
    private function generateAPIPath(APIRequest $request, RequestPolicy $requestPolicy, ClientPolicy $clientPolicy)
    {
        $urlResult = "";
        $defs      = [
            $urlResult,
            $requestPolicy->requestProtocol,
            "/",
            $request->apiId->version,
            "/",
            $request->apiId->namespace,
            "/",
            $request->apiId->name,
            "/",
            $clientPolicy->appKey,
        ];
        
        return implode($defs);
    }
}
