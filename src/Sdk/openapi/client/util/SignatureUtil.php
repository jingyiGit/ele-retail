<?php

namespace JyEleRetail\Sdk\openapi\client\util;

class SignatureUtil
{
    
    public static function genSign($data, $_secret)
    {
        $arr              = [];
        $arr['body']      = $data['body'];
        $arr['cmd']       = $data['cmd'];
        $arr['encrypt']   = $data['encrypt'];
        $arr['secret']    = $_secret;
        $arr['source']    = $data['source'];
        $arr['ticket']    = $data['ticket'];
        $arr['timestamp'] = $data['timestamp'];
        $arr['version']   = $data['version'];
        ksort($arr);
        $tmp = [];
        foreach ($arr as $key => $value) {
            $tmp[] = "$key=$value";
        }
        $strSign = implode('&', $tmp);
        
        return strtoupper(md5($strSign));
    }
}
