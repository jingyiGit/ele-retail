<?php

namespace JyEleRetail\Sdk;

use JyEleRetail\Sdk\openapi\client\policy\ClientPolicy;
use JyEleRetail\Sdk\openapi\client\SyncAPIClient;
use JyEleRetail\Sdk\openapi\client\policy\RequestPolicy;
use JyEleRetail\Sdk\openapi\client\policy\DataProtocol;
use JyEleRetail\Sdk\openapi\client\entity\AuthorizationToken;
use JyEleRetail\Sdk\openapi\client\APIRequest;
use JyEleRetail\Sdk\openapi\client\util\SignatureUtil;

class Facade
{
    private $serverHost = "api-be.ele.me";
    private $httpPort = 80;
    private $httpsPort = 443;
    private $appKey;
    private $secKey;
    private $syncAPIClient;
    
    public function setServerHost($serverHost)
    {
        $this->serverHost = $serverHost;
    }
    
    public function setHttpPort($httpPort)
    {
        $this->httpPort = $httpPort;
    }
    
    public function setHttpsPort($httpsPort)
    {
        $this->httpsPort = $httpsPort;
    }
    
    public function setAppKey($appKey)
    {
        $this->appKey = $appKey;
    }
    
    public function setSecKey($secKey)
    {
        $this->secKey = $secKey;
    }
    
    public function initClient()
    {
        $clientPolicy             = new ClientPolicy ();
        $clientPolicy->appKey     = $this->appKey;
        $clientPolicy->secKey     = $this->secKey;
        $clientPolicy->httpPort   = $this->httpPort;
        $clientPolicy->httpsPort  = $this->httpsPort;
        $clientPolicy->serverHost = $this->serverHost;
        $this->syncAPIClient      = new SyncAPIClient ($clientPolicy);
    }
    
    public function getAPIClient()
    {
        if ($this->syncAPIClient == null) {
            $this->initClient();
        }
        return $this->syncAPIClient;
    }
    
    /**
     * 获取 授权access token
     *
     * @param $code
     * @return AuthorizationToken
     */
    public function getToken($code)
    {
        $reqPolicy                       = new RequestPolicy();
        $reqPolicy->httpMethod           = "POST";
        $reqPolicy->needAuthorization    = false;
        $reqPolicy->requestSendTimestamp = true;
        $reqPolicy->useHttps             = true;
        $reqPolicy->requestProtocol      = DataProtocol::param2;
        
        $url = sprintf("https://open-auth.ele.me/oauth/token?grant_type=authorization_code&need_refresh_token=true&client_id=%s&client_secret=%s&code=%s", $this->appKey, $this->secKey, $code);
        
        $resultDefinition = new AuthorizationToken ();
        $result           = $this->getAPIClient()->send($url, "");
        $result           = json_decode($result);
        $resultDefinition->setStdResult($result);
        return $resultDefinition;
    }
    
    /**
     * 刷新token
     *
     * @param $refreshToken
     * @return AuthorizationToken
     */
    public function refreshToken($refreshToken)
    {
        $url              = sprintf("https://open-auth.ele.me/oauth/token?grant_type=refresh_token&client_id=%s&client_secret=%s&refresh_token=%s", $this->appKey, $this->secKey, $refreshToken);
        $resultDefinition = new AuthorizationToken ();
        $result           = $this->getAPIClient()->send($url, "");
        $result           = json_decode($result);
        $resultDefinition->setStdResult($result);
        return $resultDefinition;
    }
    
    private function _buildCmd(APIRequest $request)
    {
        if (empty($ticket)) {
            $ticket = $this->_genTicket();
        }
        
        $req              = [];
        $req['cmd']       = $request->apiId->name;
        $req['source']    = $this->appKey;
        $req['secret']    = $this->secKey;
        $req['ticket']    = $ticket;
        $req['version']   = $request->apiId->version;
        $req['encrypt']   = $request->encrypt;
        $req['timestamp'] = time();
        $req['body']      = json_encode($request->requestEntity);
        $req['sign']      = SignatureUtil::genSign($req, $this->secKey);
        $req['body']      = urlencode($req['body']);
        $dataR            = [];
        foreach ($req as $key => $v) {
            $dataR[] = "$key=$v";
        }
        
        return implode('&', $dataR);
    }
    
    /**
     * 生成ticket
     */
    private function _genTicket()
    {
        $uuid = '';
        if (function_exists('com_create_guid')) {
            $uuid = trim(com_create_guid(), '{}');
        } else {
            mt_srand((double)microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);
            $uuid   = substr($charid, 0, 8) . $hyphen
                      . substr($charid, 8, 4) . $hyphen
                      . substr($charid, 12, 4) . $hyphen
                      . substr($charid, 16, 4) . $hyphen
                      . substr($charid, 20, 12);
        }
        return strtoupper($uuid);
    }
    
    public function request(array $param, $apiId, $accessToken = '')
    {
        $request                = new APIRequest ();
        $request->apiId         = $apiId;
        $request->requestEntity = (object)$param;
        $request->version       = $apiId->version;
        $request->accessToken   = $accessToken;
        $requestData            = $this->_buildCmd($request);
        $result                 = $this->getAPIClient()->send($this->serverHost, $requestData);
        return json_decode($result, true);
    }
}
