<?php

namespace JyEleRetail\EleRetail;

use JyEleRetail\Sdk\openapi\client\APIId;

/**
 * 商品
 */
trait Good
{
    /**
     * 取商品列表
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:sku.list-3?aopApiCategory=item_all&type=api_menu
     */
    public function getGoodList($param)
    {
        $apiId = new APIId ("me.ele.retail", "sku.list", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取店铺内分类下商品
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:sku.shop.customsku.list-3?aopApiCategory=item_all&type=api_menu
     */
    public function getGoodListByType($param)
    {
        $apiId = new APIId ("me.ele.retail", "sku.shop.customsku.list", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 根据店铺内分类获取商品
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:sku.getItemsByCategoryId-3?aopApiCategory=item_all&type=api_menu
     */
    public function getGoodListByCategoryId($param)
    {
        $apiId = new APIId ("me.ele.retail", "sku.getItemsByCategoryId", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 取店铺内分类列表
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:sku.shop.category.get-3?aopApiCategory=item_all&type=api_menu
     */
    public function getTypeList($param)
    {
        $apiId = new APIId ("me.ele.retail", "sku.shop.category.get", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取商品类目列表
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:sku.category.list-3?aopApiCategory=item_all&type=api_menu
     */
    public function getCategoryList($param)
    {
        $apiId = new APIId ("me.ele.retail", "sku.category.list", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
}
