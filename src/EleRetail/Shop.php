<?php

namespace JyEleRetail\EleRetail;

use JyEleRetail\Sdk\openapi\client\APIId;

/**
 * 店铺
 */
trait Shop
{
    /**
     * 查看商户的营业状态
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.108a225f4ohnSR#/apidoc/me.ele.retail:shop.busstatus.get-3?aopApiCategory=shop_all&type=api_menu
     */
    public function getShopStatus($param)
    {
        $apiId = new APIId ("me.ele.retail", "shop.busstatus.get", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 查看商户信息
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:shop.get-3?aopApiCategory=shop_all&type=api_menu
     */
    public function getShopInfo($param)
    {
        $apiId = new APIId ("me.ele.retail", "shop.get", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 取商户列表
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:shop.list-3?aopApiCategory=shop_all&type=api_menu
     */
    public function getShopList($param)
    {
        $apiId = new APIId ("me.ele.retail", "shop.list", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 商户开业
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:shop.open-3?aopApiCategory=shop_all&type=api_menu
     */
    public function setShopOpen($param)
    {
        $apiId = new APIId ("me.ele.retail", "shop.open", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 商户休息
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:shop.close-3?aopApiCategory=shop_all&type=api_menu
     *
     * @remark 用于临时设置商户营业状态为休息中（商家端展示为休息中，用户端显示为休息中或暂停营业）。
     */
    public function setShopClose($param)
    {
        $apiId = new APIId ("me.ele.retail", "shop.close", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
}
