<?php

namespace JyEleRetail\EleRetail;

use JyEleRetail\Sdk\openapi\client\APIId;

/**
 * 订单
 */
trait Order
{
    /**
     * 查看订单详情
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.get-3?aopApiCategory=order_all&type=api_menu
     *
     * @return false|mixed
     */
    public function getOrderDetails($param)
    {
        $apiId = new APIId ("me.ele.retail", "order.get", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 查看订单列表
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.list-3?aopApiCategory=order_all&type=api_menu
     *
     * @return false|mixed
     */
    public function getOrderList($param)
    {
        $apiId = new APIId ("me.ele.retail", "order.list", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 取订单隐私信息
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.privateinfo-3?aopApiCategory=order_all&type=api_menu
     *
     * @return false|mixed
     */
    public function getOrderPrivateinfo($param)
    {
        $apiId = new APIId ("me.ele.retail", "order.privateinfo", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 确认订单
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.confirm-3?aopApiCategory=order_all&type=api_menu
     *
     * @remark 帮商家接单
     */
    public function orderConfirm($param)
    {
        $apiId = new APIId ("me.ele.retail", "order.confirm", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 自配送接入骑手状态
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.selfDeliveryStateSync-3?aopApiCategory=order_all&type=api_menu
     */
    public function selfDeliveryStateSync($param)
    {
        $param['distributor_id'] = '201';
        
        if (!isset($param['distributorTypeId'])) {
            $param['distributorTypeId'] = '99999'; // 其他配送
        }
        
        $apiId = new APIId ("me.ele.retail", "order.selfDeliveryStateSync", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 自配送接入骑手轨迹
     * https://open-retail.ele.me/?spm=a2f86.b661348.0.0.6242225frLja3w#/apidoc/me.ele.retail:order.selfDeliveryLocationSync-3?aopApiCategory=order_all&type=api_menu
     */
    public function selfDeliveryLocationSync($param)
    {
        if (!isset($param['location']['UTC'])) {
            $param['location']['UTC'] = time(); // 当前时间，格式：10位时间戳
        }
        if (!isset($param['location']['altitude'])) {
            $param['location']['altitude'] = 20; // 海拔高度
        }
        $apiId = new APIId ("me.ele.retail", "order.selfDeliveryLocationSync", 3);
        $res   = $this->getApp()->request($param, $apiId);
        return $this->handleReturn($res);
    }
    
    /**
     * 同步配送信息
     *
     * @remark selfDeliveryStateSync 和 selfDeliveryLocationSync 的组合
     * @param array $param
     * @return bool
     */
    public function riderPosition($param)
    {
        // 同步订单的状态信息
        $data = [
            'order_id'   => $param['order_id'],
            'selfStatus' => $param['state'],
            'knight'     => [
                'id'    => $param['rider_id'] ?: 1,
                'name'  => $param['rider_name'],
                'phone' => $param['rider_phone'],
            ],
        ];
        if (!$this->selfDeliveryStateSync($data)) {
            return false;
        }
        
        // 同步订单的位置信息
        $data = [
            'order_id' => $param['order_id'],
            'location' => [
                'longitude' => $param['longitude'],
                'latitude'  => $param['latitude'],
            ],
        ];
        if (isset($param['utc'])) {
            $data['location']['UTC'] = $param['utc'];
        }
        if (isset($param['altitude'])) {
            $data['location']['altitude'] = $param['altitude'];
        }
        if (!$this->selfDeliveryLocationSync($data)) {
            return false;
        }
        return true;
    }
}
