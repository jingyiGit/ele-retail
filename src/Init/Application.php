<?php

namespace JyEleRetail\Init;

use JyEleRetail\Kernel\Response;
use JyEleRetail\EleRetail\EleRetail;
use JyEleRetail\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends EleRetail
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
